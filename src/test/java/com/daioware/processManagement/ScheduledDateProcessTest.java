package com.daioware.processManagement;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ScheduledDateProcessTest {
	
	private static void proc() {
		System.out.println("Executing process");
	}
	public static void main(String[] args) {
		DateTimeFormatter dateFormat
			=DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
		ProcessManager manager=new ProcessManager();
		ScheduledDateProcess proc=new ScheduledDateProcess(ScheduledDateProcessTest::proc);
		ZonedDateTime zonedDate=ZonedDateTime.now();
		System.out.println("Starting main at "+dateFormat.format(zonedDate));
		for(int i=0;i<3;i++) {
			zonedDate=zonedDate.plusHours(1);
			proc.addDate(zonedDate.plusNanos(0));
		}
		proc.setFormatSleepingDuration(Process.FORMAT_SECONDS);
		proc.setSleepBetweenRuns(false);
		manager.startProcess(proc);
	}
}
