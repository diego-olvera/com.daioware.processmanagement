package com.daioware.processManagement;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import com.daioware.dateUtilities.schedule.ExecutionHour;

public class DailyHourProcessTest {

	public static void main(String[] args) {
		DailyHourProcess proc=new DailyHourProcess(()->System.out.println("Hello =)"));
		proc.setFormatSleepingDuration(Process.FORMAT_MINUTES);
		ZonedDateTime date=ZonedDateTime.now(ZoneId.systemDefault());
		int currentHour=date.getHour();
		proc.addHour(new ExecutionHour(currentHour++));
		proc.addHour(new ExecutionHour(currentHour++,30));
		proc.start();
	}
}
