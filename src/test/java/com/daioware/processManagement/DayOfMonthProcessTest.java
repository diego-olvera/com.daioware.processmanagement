package com.daioware.processManagement;

import java.time.ZonedDateTime;

import com.daioware.stream.Printer;

public class DayOfMonthProcessTest {

	public static void main(String args[]) {
		Printer printer=Printer.defaultPrinter;
		Runnable procedure=()->System.out.println("Running procedure =)");
		String name="Day Of Month Process";
		DayOfMonthProcess proc=new DayOfMonthProcess(procedure, printer, name);
		ZonedDateTime date=ZonedDateTime.now();
		proc.add(new DayOfMonth(date.getMonth(),(short)date.getDayOfMonth()));
		proc.add(new DayOfMonth(date.getMonth(),(short)date.getDayOfMonth()+1));
		proc.start();
	}
}
