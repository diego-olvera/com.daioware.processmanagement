package com.daioware.processManagement;

import java.time.DayOfWeek;

import com.daioware.dateUtilities.CurrentDate;
import com.daioware.dateUtilities.schedule.DayAndHourExec;
import com.daioware.dateUtilities.schedule.ExecutionHour;
import com.daioware.dateUtilities.schedule.Week;
import com.daioware.stream.Printer;

public class DayProcess extends Process{
	private Week week=new Week();
	
	public DayProcess(Runnable proc, Printer printer, String name, long sleepingMilis) {
		super(proc, printer, name, sleepingMilis);
	}

	public DayProcess(Runnable proc, String name) {
		super(proc, name);
	}

	public DayProcess(Runnable proc) {
		super(proc);
	}
	
	public DayProcess shallowCopy() {
		synchronized (this) {
			DayProcess copy=new DayProcess(getRunnable());
			copy(this,copy);
			copy.week=week;
			return copy;
		}		
	}
	public Week getWeek() {
		return week;
	}

	public void setWeek(Week week) {
		this.week = week;
	}

	@Override
	public boolean isTimeToRun() {
		DayOfWeek currentWeekDay=CurrentDate.getWeekDay();
		DayAndHourExec day=week.getDay(currentWeekDay);
		ExecutionHour hour;
		boolean isTime;
		int currentMinutes=CurrentDate.getMinutes();
		int currentHour=CurrentDate.getHour();
		boolean hourExecuted;
		boolean minutesCorrect;
		int hourMinutes;
		if(day==null || day.isFinished()) {
			isTime= false;
			println("Updating sleeping time for next day");
			setSleepingMinutes(
					(int)((new ExecutionHour(23,59)).getTotalMinutesOfDay()-CurrentDate.getMinutesOfDay()));
		}
		else {
			hour=day.getHour(currentHour);
			if(hour==null) {
				isTime= false;
				println("Updating sleeping time for next hour because current one is null");
				setSleepingMinutes(60-currentMinutes);
			}
			else {
				minutesCorrect=currentMinutes>=(hourMinutes=hour.getMinute());
				if(!(hourExecuted=hour.isExecuted()) && minutesCorrect) {
					isTime= true;
					println("Updating sleeping time for next hour");
					setSleepingMinutes(60-currentMinutes);
				}
				else if(!hourExecuted && !minutesCorrect) {
					println("Updating sleeping time for current hour");
					setSleepingMinutes(hourMinutes-currentMinutes);
					isTime= false;
				}
				else {
					isTime= false;
					println("Updating sleeping time for next hour");
					setSleepingMinutes(60-currentMinutes);
				}
			}		
		}	
		if(!isTime) {
			updateWeekDaysDifferentTo(currentWeekDay);
		}
		return isTime;
	}

	protected void updateWeekDaysDifferentTo(DayOfWeek weekDay) {
		for(DayAndHourExec dayHour:week.getDays().values()){
			if(!dayHour.getDay().equals(weekDay)) {
				for(ExecutionHour hourAux:dayHour.getHours().values()) {
					hourAux.setExecuted(false);
				}
				dayHour.setFinished(false);
			}
		}
	}

	@Override
	public void setRunningFields() {
		try {
			DayOfWeek currentWeekDay=CurrentDate.getWeekDay();
			DayAndHourExec day=week.getDay(currentWeekDay);
			ExecutionHour hour=day.getHour(CurrentDate.getHour());
			hour.setExecuted(true);
			if(day.finishedAllHours()) {
				day.setFinished(true);
			}
			updateWeekDaysDifferentTo(currentWeekDay);
		}catch(Exception e) {
			println("ERROR:"+e.getMessage());
		}	
	}
}
