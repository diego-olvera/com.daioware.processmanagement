package com.daioware.processManagement;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.daioware.stream.Printer;

public class ProcessManager extends Process implements Iterable<Process>{
	
	private Map<String,Process> processes=Collections.synchronizedMap(new HashMap<String,Process>());
	
	public ProcessManager() {
		this(System.out::print);
	}
	
	public ProcessManager(Printer printer) {
		super(null,printer,"Process Manager",0);
		setRunnable(()->checkProcesses());
	}
	public static Printer getEmptyPrinter() {
		return emptyPrinter;
	}

	public Iterable<Process> getProcessesIterable() {
		return processes.values();
	}
	@Override
	public Iterator<Process> iterator() {
		return getProcessesIterable().iterator();
	}
	public boolean addProcess(Process p) {
		String name=p.getName();
		if(processes.containsKey(name)) {
			return false;
		}
		return processes.put(name,p)==null;
	}
	public Process getProcess(String name) {
		return processes.get(name);
	}
	public Process removeProcess(String name) {
		return processes.remove(name);
	}

	public boolean removeProcess(Process p) {
		return processes.remove(p.getName())!=null;
	}
	@SuppressWarnings("deprecation")
	public boolean terminateProcess(Process p) {
		if(p==null)return false;
		String name=p.getName();
		if(p.getState().equals(State.TERMINATED)) {
			println("Thread '"+name+"' already stop");
			return true;
		}
		stopProcess(p,false);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(p.getState().equals(State.TERMINATED)) {
			println("Thread '"+name+"' stop by interruption");
			return true;
		}
		else{
			println("Forcing to stop thread '"+p.getName()+"'");
			p.stop();
			if(p.getState().equals(State.BLOCKED)) {
				println("Thread '"+name+"' blocked, using join");
				try {
					p.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if(!p.getState().equals(State.TERMINATED)) {
				println("Thread '"+name+"' still running");
				return false;
			}
			return true;
		}
	}
	public boolean terminateProcess(String name) {
		return terminateProcess(getProcess(name));
	}
	public boolean stopProcess(String name) {
		return stopProcess(name,true);
	}
	public boolean stopProcess(String name,boolean waitForTermination) {
		return stopProcess(getProcess(name),waitForTermination);
	}
	public boolean stopProcess(Process p) {
		return stopProcess(p,true);
	}
	public boolean stopProcess(Process p,boolean waitForTermination) {
		if(p==null)return false;
		p.forceSetActive(false);
		if(waitForTermination) {
			while(!p.getState().equals(State.TERMINATED)) {
			}
		}		
		return true;
	}
	public boolean startProcess(String name) {
		return startProcess(getProcess(name));
	}
	public boolean startProcess(Process p) {
		if(p==null)return false;
		p.forceSetActive(true);
		p.start();
		return true;
	}
	public void startAllProcesses() {
		for(Process p:this) {
			startProcess(p);
		}
	}
	public void stopAllProcesses() {
		stopAllProcesses(true);
	}
	public void stopAllProcesses(boolean waitForTermination) {
		for(Process p:this) {
			stopProcess(p,waitForTermination);
		}		
	}
	public Process forceStart(Process p) {
		String name=p.getName();
		State state=p.getState();
		if(!state.equals(State.TERMINATED)) {
			println("Trying to force start thread '"+name+"' with state "+state);
			return null;		}
		try {
			startProcess(p);
		}catch(IllegalThreadStateException e) {			
			println("Trying to start thread '"+name+"'"+" with "+state+" state");
			if(state.equals(State.TERMINATED)) {
				p=restartProcess(p);
			}
			else {
				p=null;
				println("Thread '"+name+"' coudlnt be started");
			}
		}
		return p;
	}
	public Process restartProcess(Process oldProcess) {
		String name=oldProcess.getName();
		State state=oldProcess.getState();
		if(!state.equals(State.TERMINATED)) {
			println("Trying to restart thread '"+name+"' with state "+state);
			return oldProcess;
		}
		println("Restarting thread '"+name+"'");
		Process newProcess=oldProcess.shallowCopy();
		newProcess.setActive(true);
		
		if(!removeProcess(oldProcess)) {
			return null;
		}
		println("Thread '"+name+"' removed");
		if(!addProcess(newProcess)) {
			return null;
		}
		println("Thread '"+name+"' re-added");
		if(!startProcess(newProcess)) {
			return null;
		}	
		println("Thread '"+name+"' started");
		return newProcess;		
	}	
	public synchronized void wakeUp() {
		notify();
	}
	public void checkProcesses() {
		println("Checking processes");
		for(Process p:this) {
			if(p.getState().equals(State.TERMINATED) && p.isEnabled() && p.isTimeToRun()) {
				forceStart(p);
			}
		}	
	}	
}
