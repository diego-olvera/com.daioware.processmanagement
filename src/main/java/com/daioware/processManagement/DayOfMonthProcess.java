package com.daioware.processManagement;

import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.daioware.collections.util.Searchs;
import com.daioware.commons.wrapper.WrapperBoolean;
import com.daioware.dateUtilities.CurrentDate;
import com.daioware.stream.Printer;

public class DayOfMonthProcess extends Process{

	public static final Comparator<DayOfMonth> dayOfMonthComparator=new Comparator<DayOfMonth>() {

		@Override
		public int compare(DayOfMonth o1, DayOfMonth o2) {
			int comp=Integer.compare(o1.getMonth().getValue(),o2.getMonth().getValue());
			return comp==0?Integer.compare(o1.getDay(),o2.getDay()):comp;
		}
	};
	private List<DayOfMonth> dayOfMonths=new ArrayList<>();
	private DayOfMonth currentItemExecuting;
		
	public DayOfMonthProcess(Runnable procedure, Printer printer, String name) {
		super(procedure, printer, name, 1);
	}

	public DayOfMonthProcess(Runnable procedure, String name) {
		super(procedure, name);
	}

	public DayOfMonthProcess(Runnable procedure) {
		super(procedure);
	}
	public DayOfMonthProcess shallowCopy() {
		synchronized (this) {
			DayOfMonthProcess copy=new DayOfMonthProcess(getRunnable());
			copy(this,copy);
			for(DayOfMonth item:dayOfMonths) {
				add(item.clone());
			}
			copy.setZoneId(getZoneId());
			return copy;
		}		
	}
	public void add(DayOfMonth dayOfMonth) {
		int index=Searchs.binarySearch(dayOfMonths, dayOfMonthComparator, dayOfMonth);
		dayOfMonths.add(index<0?0:index,dayOfMonth);
	}
	public void remove(DayOfMonth dayOfMonth) {
		dayOfMonths.remove(Searchs.binarySearch(dayOfMonths, dayOfMonthComparator, dayOfMonth));
	}
	public void setRunningFields() {
		if(currentItemExecuting!=null) {
			currentItemExecuting.setExecuted(true);
			currentItemExecuting=null;
		}
	}
	protected int indexOf(Month month,WrapperBoolean found) {
		return indexOf(month,0,found);
	}
	protected int indexOf(Month month,int start,WrapperBoolean found) {
		DayOfMonth dayOfMonth;
		Month monthAux;
		for(int i=start,j=dayOfMonths.size();i<j;i++) {
			dayOfMonth=dayOfMonths.get(i);
			monthAux=dayOfMonth.getMonth();
			if(monthAux.equals(month) && !dayOfMonth.isExecuted()) {
				found.value=true;
				return i;
			}
			else if(monthAux.compareTo(month)>0){
				found.value=false;
				return i;
			}
		}
		found.value=false;
		return -1;
	}
	public boolean isTimeToRun() {
		WrapperBoolean found=new WrapperBoolean();
		ZonedDateTime date=ZonedDateTime.now(getZoneId());	
		Month currentMonth=date.getMonth();
		int currentDay=date.getDayOfMonth();
		int index=indexOf(currentMonth,found);
		DayOfMonth dayOfMonth;
		int dayRead;
		if(found.value) {
			dayOfMonth=dayOfMonths.get(index);
			dayRead=dayOfMonth.getDay();
			if(dayOfMonth.isExecuted()) {
				sleepUntilNextAvailableMonth(index+1);
				return false;
			}
			else if(dayRead==currentDay){
				currentItemExecuting=dayOfMonth;
				return true;
			}
			else {
				sleepUntilDayOfCurrentMonth(dayRead);
				return false;
			}
		}
		else if(!found.value && index>=0) {
			sleepUntilNextAvailableMonth(index);
			return false;
		}
		else {
			sleepUntilNextAvailableMonth(dayOfMonths.size());
			return false;
		}
	}

	private void sleepUntilDayOfCurrentMonth(int day) {
		ZoneId zoneId=getZoneId();
		ZonedDateTime currDate=ZonedDateTime.now(zoneId);
		setSleepingMinutes(TimeUnit.DAYS.toMinutes(day-currDate.getDayOfMonth())
				-CurrentDate.getMinutesOfDay(zoneId));
	}

	private void sleepUntilNextAvailableMonth(int startIndex) {
		ZoneId zoneId=getZoneId();
		ZonedDateTime currDate=ZonedDateTime.now(zoneId);
		boolean currLap;
		DayOfMonth dayOfMonth=(currLap=startIndex<dayOfMonths.size())?dayOfMonths.get(startIndex):dayOfMonths.get(0);
		long sleepingMinutes;
		ZonedDateTime futureDate;
		if(!currLap) {
			for(DayOfMonth m:dayOfMonths) {
				m.setExecuted(false);
			}
			futureDate=
					ZonedDateTime.of(currDate.getYear()+1,dayOfMonth.getMonth().getValue(), 
						dayOfMonth.getDay(), 0, 0,0,0,zoneId);
			sleepingMinutes=currDate.until(futureDate,ChronoUnit.MINUTES);
		}
		else {
			futureDate=ZonedDateTime.of(currDate.getYear(),dayOfMonth.getMonth().getValue(), 
					dayOfMonth.getDay(), 0, 0,0,0,zoneId).minusDays(1);
			sleepingMinutes=currDate.until(futureDate,ChronoUnit.MINUTES);
		}
		setSleepingMinutes(sleepingMinutes);
	}
}
