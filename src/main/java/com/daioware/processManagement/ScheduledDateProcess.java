package com.daioware.processManagement;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.daioware.stream.Printer;

public class ScheduledDateProcess extends Process implements Iterable<ScheduledDate>{

	private ArrayList<ScheduledDate> dates=new ArrayList<>();
	protected ScheduledDate nearestDate;
	
	public ScheduledDateProcess(Runnable procedure, Printer printer, String name, long sleepingMilis) {
		super(procedure, printer, name, sleepingMilis);
	}
	
	public ScheduledDateProcess(Runnable procedure, String name) {
		super(procedure, name);
	}

	public ScheduledDateProcess(Runnable procedure) {
		super(procedure);
	}
	public Process shallowCopy() {
		synchronized (this) {
			ScheduledDateProcess copy=new ScheduledDateProcess(getRunnable());
			copy(this,copy);
			copy.dates=dates;
			copy.nearestDate=nearestDate;
			return copy;
		}		
	}
	public void addDates(LocalDateTime... dates) {
		for(LocalDateTime date:dates) {
			addDate(date);
		}
	}
	public void addLocalDateTimesDates(List<LocalDateTime> dates) {
		for(LocalDateTime date:dates) {
			addDate(date);
		}
	}
	public void addDates(ZonedDateTime... dates) {
		for(ZonedDateTime date:dates) {
			addDate(date);
		}
	}
	public void addDates(List<ZonedDateTime> dates) {
		for(ZonedDateTime date:dates) {
			addDate(date);
		}
	}
	public boolean addDate(ZonedDateTime date) {
		return addDate(new ScheduledDate(date));
	}
	public boolean addDate(LocalDateTime date) {
		return addDate(date.atZone(getZoneId()));
	}
	public boolean addDate(ScheduledDate date) {
		return dates.add(date);
	}
	public ScheduledDate removeDate(int index) {
		return dates.remove(index);
	}
	public ScheduledDate getDate(int index) {
		return dates.get(index);
	}
	public int datesSize() {
		return dates.size();
	}

	@Override
	public Iterator<ScheduledDate> iterator() {
		return dates.iterator();
	}
	public boolean allDatesExecuted() {
		for(ScheduledDate sDate:this) {
			if(!sDate.isExecuted()) {
				return false;
			}
		}
		return true;
	}
	protected long getNextExecutionSleepTime() {
		if(datesSize()<1 || allDatesExecuted()) {
			setActive(false);
			return 1;
		}
		ZonedDateTime now=ZonedDateTime.now(getZoneId());
		nearestDate=null;
		long nowTime=now.toInstant().toEpochMilli();
		long nearestTime=-1,auxTime;
		for(ScheduledDate sDate:this) {
			if(!sDate.isExecuted()) {
				auxTime=Math.abs(nowTime-sDate.getDate().toInstant().toEpochMilli());
				if(nearestTime==-1) {
					nearestTime=auxTime;
					nearestDate=sDate;
				}
				else if(auxTime>=0 && nearestTime>auxTime) {
					nearestTime=auxTime;
					nearestDate=sDate;
				}
				//else nearestTime and nearestDate remains the same
			}			
		}
		return nearestTime;	
	}
	public void setRunningFields() {
		if(nearestDate!=null) {
			nearestDate.setExecuted(true);
			nearestDate=null;
		}
	}
	public boolean isTimeToRun() {
		if(nearestDate!=null) {
			return true;
		}
		else {
			setSleepingMillis(getNextExecutionSleepTime());
			return false;
		}		
	}
	
}
