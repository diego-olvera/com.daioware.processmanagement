package com.daioware.processManagement;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZonedDateTime;

public class DayOfMonth {
	private Month month;
	private short day;
	private boolean executed;
	
	public DayOfMonth(LocalDateTime date) {
		this(date.getMonth(),date.getDayOfMonth());
	}
	public DayOfMonth(ZonedDateTime date) {
		this(date.getMonth(),date.getDayOfMonth());
	}
	public DayOfMonth(Month month) {
		this(month,(short)1);
	}
	public DayOfMonth(Month month, int day) {
		this(month,(short)day);
	}
	public DayOfMonth(Month month, short day) {
		setMonth(month);
		setDay(day);
	}
	public DayOfMonth(DayOfMonth item) {
		this(item.getMonth(),item.getDay());
	}
	public DayOfMonth clone() {
		return new DayOfMonth(this);
	}
	public Month getMonth() {
		return month;
	}
	public short getDay() {
		return day;
	}
	public boolean isExecuted() {
		return executed;
	}
	public void setMonth(Month month) {
		this.month = month;
	}
	public void setDay(int day) {
		setDay((short)day);
	}
	public void setDay(short day) {
		this.day = day;
	}
	public void setExecuted(boolean executed) {
		this.executed = executed;
	}
}
