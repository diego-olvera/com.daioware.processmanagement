package com.daioware.processManagement;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map.Entry;

import com.daioware.dateUtilities.schedule.ExecutionHour;
import com.daioware.stream.Printer;

public class DailyHourProcess extends Process{
	private HashMap<Integer,ExecutionHour> hours=new HashMap<>();
	
	private ZoneId zoneId=ZoneId.systemDefault();
	
	public DailyHourProcess(Runnable proc, Printer printer, String name, long sleepingMilis) {
		super(proc, printer, name, sleepingMilis);
	}

	public DailyHourProcess(Runnable proc, String name) {
		super(proc, name);
	}
	public DailyHourProcess(Runnable proc) {
		super(proc);
	}
	public Process shallowCopy() {
		synchronized (this) {
			DailyHourProcess copy=new DailyHourProcess(getRunnable(), getPrinter(), getName(), getSleepingMillis());
			copy.setFormatSleepingDuration(getFormatSleepingDuration());
			copy.hours=hours;
			return copy;
		}		
	}
	
	public ZoneId getZoneId() {
		return zoneId;
	}

	public void setZoneId(ZoneId zoneId) {
		this.zoneId = zoneId;
	}

	public ExecutionHour addHour(ExecutionHour hour) {
		return hours.put(hour.getHour(),hour);
	}
	public ExecutionHour removeHour(int hour) {
		return hours.remove(hour);
	}
	public ExecutionHour getHour(int hour) {
		return hours.get(hour);
	}
	public void addHours(int firstHour,int endHour,int min) {
		for(int hour=firstHour;hour<=endHour;hour++) {
			addHour(new ExecutionHour(hour, min));
		}
	}
	public void addHours(int firstHour,int endHour) {
		addHours(firstHour,endHour,0);
	}
	public void addHours(int firstHour) {
		addHours(firstHour,23,0);
	}
	public void addHours() {
		addHours(0,23);
	}
	protected int getNextHourMinutes(int hour) {
		ExecutionHour exec=null;
		while(hour<=23) {
			exec=hours.get(++hour);
			if(exec!=null)break;
		}
		return exec!=null?exec.getMinute():0;
	}
	@Override
	public boolean isTimeToRun() {
		ZonedDateTime date=ZonedDateTime.now(getZoneId());
		int currentHour=date.getHour();
		ExecutionHour hour=hours.get(currentHour);
		int currentMinutes=date.getMinute();
		boolean isTime;
		int hourMinute;
		if(hour==null) {
			setSleepingMinutes(60-currentMinutes);
			isTime=false;
		}
		else if(currentMinutes<(hourMinute=hour.getMinute())) {
			setSleepingMinutes(hourMinute-currentMinutes);
			isTime=false;
		}
		else {
			isTime=true;
			setSleepingMinutes((60-currentMinutes)+getNextHourMinutes(currentHour));
		}
		return isTime;
	}
	@Override
	public void setRunningFields() {
		int currentHour=ZonedDateTime.now(getZoneId()).getHour();
		for(Entry<Integer,ExecutionHour> entry:hours.entrySet()) {
			entry.getValue().setExecuted(entry.getKey()==currentHour);
		}
	}
}
