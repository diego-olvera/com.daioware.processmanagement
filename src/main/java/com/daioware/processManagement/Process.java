package com.daioware.processManagement;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.function.Consumer;

import com.daioware.dateUtilities.TimeInterval;
import com.daioware.log.ProcessLogger;
import com.daioware.math.Number;
import com.daioware.stream.Printer;

public class Process extends Thread{
	public static final int FORMAT_MILLIS=0;
	public static final int FORMAT_SECONDS=1;

	public static final int FORMAT_MINUTES=2;
	public static final int FORMAT_HOURS=3;
	public static final int FORMAT_DAYS=4;
	public static final DateTimeFormatter defaultFormat
			=DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");

	public static final ProcessLogger emptyLogger=new ProcessLogger() {		
		@Override
		public void storeTimeIntervals(Instant startDate, Instant finishDate) {				
		}

		@Override
		public Iterable<TimeInterval> getTimeIntervalIterator() {
			return Collections.emptyList();
		}

		@Override
		public long getRunningTimes() {
			return 0;
		}
		@Override
		public void emptyLog() {			
		}
	};
	public static final Printer emptyPrinter=new Printer() {		
		@Override
		public void print(Object o) {			
		}
	};

	public static final Consumer<Exception> defaultExceptionConsumer=(x)->{};
	
	private ZoneId zoneId=ZoneId.systemDefault();
	private Instant startedDate;
	private Instant terminatedDate;
	private Runnable procedure;
	private Printer printer;
	private long sleepingMilis;
	private int formatSleepingDuration=FORMAT_SECONDS;
	private boolean active;
	private boolean sleepBetweenRuns=true;
	private boolean enabled=true;
	private DateTimeFormatter simpleDateFormat=defaultFormat;
	private ProcessLogger threadLogger;
	private int execTimes;
	private Consumer<Exception> exceptionConsumer;
	
	public Process(Runnable procedure) {
		this(procedure,"Unknown thread");
	}
	public Process(Runnable procedure,String name) {
		this(procedure,System.out::print,name,2000);
	}
	public Process(Runnable procedure, Printer printer,String name,long sleepingMillis) {
		setRunnable(procedure);
		setPrinter(printer);
		setName(name);
		setSleepingMillis(sleepingMillis);
		setActive(true);
		setThreadLogger(null);
		setExceptionConsumer(defaultExceptionConsumer);
	}
	public Process shallowCopy() {
		synchronized (this) {
			Process process=new Process(getProcedure());
			return copy(this,process);
		}		
	}
	public ZoneId getZoneId() {
		return zoneId;
	}

	public void setZoneId(ZoneId zoneId) {
		this.zoneId = zoneId;
	}
	public Consumer<Exception> getExceptionConsumer() {
		return exceptionConsumer;
	}
	public void setExceptionConsumer(Consumer<Exception> exceptionConsumer) {
		this.exceptionConsumer = exceptionConsumer;
	}
	public Runnable getProcedure() {
		return procedure;
	}
	
	public void setProcedure(Runnable procedure) {
		this.procedure = procedure;
	}
	public static Process copy(Process source,Process dest) {
		dest.setName(source.getName()+"_copy");
		dest.setFormatSleepingDuration(source.getFormatSleepingDuration());
		dest.setStartedDate(source.getStartedDate());
		dest.setTerminatedDate(source.getTerminatedDate());
		dest.setProcedure(source.getProcedure());
		dest.setPrinter(source.getPrinter());
		dest.setSleepingMillis(source.getSleepingMillis());
		dest.setFormatSleepingDuration(source.getFormatSleepingDuration());
		dest.setActive(source.isActive());
		dest.setSleepBetweenRuns(source.isSleepBetweenRuns());
		dest.setEnabled(source.isEnabled());
		dest.setSimpleDateFormat(source.getSimpleDateFormat());
		return dest;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public static ProcessLogger getEmptyLogger() {
		return emptyLogger;
	}
	public static Printer getEmptyPrinter() {
		return emptyPrinter;
	}
	public ProcessLogger getThreadLogger() {
		return threadLogger;
	}
	public void setThreadLogger(ProcessLogger t) {
		if(t==null) {
			t=getEmptyLogger();
		}
		threadLogger = t;
	}
	public boolean isSleepBetweenRuns() {
		return sleepBetweenRuns;
	}
	public void setSleepBetweenRuns(boolean sleepBetweenRuns) {
		this.sleepBetweenRuns = sleepBetweenRuns;
	}
	public void println(Object o) {
		printer.println(o);
	}
	public void print(Object o) {
		printer.println(o);
	}
	public long getExecTimes() {
		return execTimes;
	}
	public Instant getTerminatedDate() {
		return terminatedDate;
	}
	protected void setTerminatedDate(Instant terminatedDate) {
		this.terminatedDate = terminatedDate;
	}
	
	public Instant getStartedDate() {
		return startedDate;
	}
	protected void setStartedDate(Instant startedDate) {
		this.startedDate = startedDate;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
		String threadName=getName();
		println((active)?"Activating thread '"+threadName+"'":"Deactivating thread '"+threadName+"'");
	}
	public void forceSetActive(boolean active) {
		setActive(active);
		interrupt();
	}
	public DateTimeFormatter getSimpleDateFormat() {
		return simpleDateFormat;
	}
	public void setSimpleDateFormat(DateTimeFormatter simpleDateFormat) {
		this.simpleDateFormat = simpleDateFormat;
	}
	public Printer getPrinter() {
		return printer;
	}
	public void setPrinter(Printer p) {
		if(p==null) {
			p=getEmptyPrinter();
		}
		printer = p;
	}
	public Runnable getRunnable() {
		return procedure;
	}
	public void setRunnable(Runnable p) {
		this.procedure = p;
	}
	public Iterable<TimeInterval> timeIntervalsIterable() {
		return threadLogger.getTimeIntervalIterator();
	}
	public int getFormatSleepingDuration() {
		return formatSleepingDuration;
	}
	public void setFormatSleepingDuration(int formatSleepingDuration) {
		this.formatSleepingDuration = formatSleepingDuration;
	}
	public double getSleepingDuration() {
		switch(getFormatSleepingDuration()) {
			case FORMAT_DAYS:return getSleepingDays();
			case FORMAT_HOURS:return getSleepingHours();
			case FORMAT_SECONDS:return getSleepingSeconds();
			case FORMAT_MINUTES:return getSleepingMinutes();
			case FORMAT_MILLIS:default: return getSleepingMillis();
		}
	}
	public String getSleepingUnit() {
		switch(getFormatSleepingDuration()) {
			case FORMAT_DAYS:return "days";
			case FORMAT_HOURS:return "hours";
			case FORMAT_SECONDS:return "seconds";
			case FORMAT_MINUTES:return "minutes";
			case FORMAT_MILLIS:default: return "miliseconds";
		}
	}
	public String getSleepingSentence() {
		return String.format("%s %s",Number.formatWithoutUnnecessaryZeros(getSleepingDuration()),
				getSleepingUnit());
	}
	public String getFormattedDate(Instant date) {
		return date!=null?getSimpleDateFormat().format(date.atZone(ZoneId.systemDefault())):null;
	}	
	public void pause() {
		setSleepingMillis(0);
	}
	public long getSleepingMillis() {
		return sleepingMilis;
	}
	public double getSleepingSeconds() {
		return ((double)getSleepingMillis())/1000;
	}
	public double getSleepingMinutes() {	
		return getSleepingSeconds()/ 60;
	}
	public double getSleepingHours() {
		return getSleepingMinutes()-60;
	}
	public double getSleepingDays() {
		return getSleepingHours()/24;
	}
	public void setSleepingMillis(long sleepingMillis) {
		this.sleepingMilis = sleepingMillis;
	}
	public void setSleepingSeconds(double value) {
		setSleepingMillis((long) (value*1000));
	}
	public void setSleepingMinutes(double value) {
		setSleepingSeconds(value*60);
	}
	public void setSleepingHours(double value) {
		setSleepingMinutes(value*60);
	}
	public void setSleepingDays(double value) {
		setSleepingHours(value*24);
	}
	public boolean isTimeToRun() {
		return true; 
	}
	public void setRunningFields() {
		
	}
	public void emptyLog() {
		threadLogger.emptyLog();
	}
	@Override
	public int hashCode() {
		return getName().hashCode();
	}
	public boolean equals(Object o) {
		return o instanceof Process?getName().equals(((Process)o).getName()):false;
	}
	public synchronized void run() {
		Instant startDate;
		Instant finishDate;
		boolean isTimeToSleep;
		long sleepingMillis;
		Consumer<Exception> exceptionConsumer;
		setStartedDate(Instant.now());
		println("Thread '"+getName()+"' starting life cicle at "+getFormattedDate(getStartedDate())); 
		while(isActive() && isEnabled()) {
			if(isActive() && isTimeToRun()) {
				isTimeToSleep=false;
				startDate=Instant.now();
				println("Thread '"+getName()+"' running at "+getFormattedDate(startDate)); 
				setRunningFields();
				try {
					procedure.run();
				}catch(Exception e) {
					exceptionConsumer=getExceptionConsumer();
					if(exceptionConsumer!=null) {
						exceptionConsumer.accept(e);
					}
					getPrinter().printStackTrace(e);
				}
				finishDate=Instant.now();
				println("Thread '"+getName()+"' finished running "+getFormattedDate(finishDate));
				threadLogger.storeTimeIntervals(startDate, finishDate);
				execTimes++;
			}
			else {
				isTimeToSleep=true;
			}
			if(isActive() && isSleepBetweenRuns() || isTimeToSleep) {
				try {
					sleepingMillis=getSleepingMillis();
					println("Thread '"+getName()+"' sleeping for "+getSleepingSentence()); 
					println("Thread '"+getName()+"' will wake up again at "
							+getFormattedDate(Instant.now().plusMillis(sleepingMillis)));
					wait(sleepingMillis);
				} catch (InterruptedException e) {
					println("Thread '"+getName()+"' interrupted while waiting"); 
				}
			}			
		}
		setTerminatedDate(Instant.now());
		println("Thread '"+getName()+"' terminated at "+getFormattedDate(getTerminatedDate())); 
		println("Thread '"+getName()+"' executed "+getExecTimes()+ " times"); 
	}
}
