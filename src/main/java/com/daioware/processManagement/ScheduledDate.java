package com.daioware.processManagement;

import java.time.ZonedDateTime;

public class ScheduledDate {
	private ZonedDateTime date;
	private boolean executed;
	
	public ScheduledDate(ZonedDateTime date) {
		this(date,false);
	}
	public ScheduledDate(ZonedDateTime date, boolean executed) {
		setDate(date);
		setExecuted(executed);
	}
	public ZonedDateTime getDate() {
		return date;
	}
	public void setDate(ZonedDateTime date) {
		this.date = date;
	}
	public boolean isExecuted() {
		return executed;
	}
	public void setExecuted(boolean executed) {
		this.executed = executed;
	}
	
	
}
