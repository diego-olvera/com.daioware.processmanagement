package com.daioware.processManagement;

import com.daioware.stream.Printer;

public class NumTimesProcess extends Process{
	
	private int maxTimesToExec=1;
	
	public NumTimesProcess(Runnable procedure, Printer printer, String name, long sleepingMillis) {
		super(procedure, printer, name, sleepingMillis);
	}

	public NumTimesProcess(Runnable procedure, String name) {
		super(procedure, name);
	}

	public NumTimesProcess(Runnable procedure) {
		super(procedure);
	}

	public int getMaxTimesToExec() {
		return maxTimesToExec;
	}

	public void setMaxTimesToExec(int maxTimesToExec) {
		this.maxTimesToExec = maxTimesToExec;
	}

	@Override
	public boolean isActive() {
		if(getExecTimes()>=getMaxTimesToExec()) {
			setActive(false);
			return false;
		}
		return super.isActive();
	}
	public static void main(String[] args) {
		NumTimesProcess proc=new NumTimesProcess(()->System.out.println("Hello =)"));
		proc.setMaxTimesToExec(2);
		proc.start();
	}
}
